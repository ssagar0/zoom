import zoom_client

def entrypoint():
    zoom_client.cli()

if __name__ == '__main__':
    entrypoint()
