# Zoom

Multi-resource project management tool

### Terms

#### zoomfile (zoom.yml)

Added by `zoom init` and sits in the root directory of the zoom project. Contains a list of all resources used by this zoom project.

#### resource

Any external resource needed by this project. Whether it is a git repo, a file, etc.

#### dependency

Non-SCM files needed by this project. Currently supported dependencies: files.

#### module

SCM repos needed by this project. Currently supported modules: git.

### Commands

#### `zoom init`

Create a new git/zoom project.

#### `zoom add git [--commit COMMIT | --branch BRANCH | --tag TAG] URL [PATH]`

Add a git module to a zoom project. By default, the module will be placed relative to the cwd in a directory that matches the name of the git repo. If COMMIT or TAG are set, zoom sync will lock to that respective commit or tag. If BRANCH is set, zoom sync will track that branch, meaning whenever a sync occurs, all of the latest upstream changes will be pulled into that module.

Tracking a branch is great for development but bad for creating a build from the past. Branches get deleted, heads of branches can move, etc. To create older, non-bleeding-edge builds (or CI builds), use `zoom sync -e` to sync to the *exact* commit specified in the zoomfile.

#### `zoom add file [--filename FILENAME] URL [PATH]`

Add a file dependency to a zoom project. By default, the dependency will be placed relative to the cwd in a directory that matches the name of the file (with extensions removed). If FILENAME is set, zoom sync will download this file and rename it FILENAME.

#### `zoom list`

List all resources added to this project.

#### `zoom sync [--force] [--exact] [LIMIT [LIMIT...]]`

Ensures all resources have been resolved to the whatever state they are locked to (or tracking, in the case of git modules). If `--force` is set, zoom will delete the resource before attempting a fresh sync. If a limit is passed, zoom will limit syncing to the specified paths. If `--exact` is used, zoom will sync all modules to the exact commit stored in the zoomfile, even if that module is tracking a branch.

#### `zoom foreach COMMAND [LIMIT [LIMIT...]]`

Run COMMAND across all modules. If a limit is specified, zoom will limit syncing to the specified paths.