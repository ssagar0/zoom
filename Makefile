
PRJ=zoom
TEST=tests

VERSION=$(shell git describe)
VERSION_FILE=$(PRJ)/__version__.py

DIST=dist
MANIFEST=MANIFEST
COVERAGE=.coverage
PACKAGE=$(DIST)/$(PRJ)-$(VERSION).tar.gz

SRC = $(shell find $(PRJ) -type f -name '*.py')
PYC = $(shell find . -type f -name '*.pyc')

.PHONY: all
all: $(PACKAGE)

.PHONY: env
env:
	pip install nose coverage mock

.PHONY: install
install: all
	pip install --upgrade $(PACKAGE)

.PHONY: uninstall
uninstall:
	pip uninstall $(PRJ)

.PHONY: test
test:
	nosetests $(TEST) -v --with-coverage --cover-package=$(PRJ)

$(VERSION_FILE):
	@echo "__version__ = \"$(VERSION)\"" > $(VERSION_FILE)

$(PACKAGE): $(VERSION_FILE) $(SRC)
	python setup.py sdist

clean:
	rm -rf $(VERSION_FILE) $(DIST) $(MANIFEST) $(PYC) $(COVERAGE)
