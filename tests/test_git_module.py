import os
import sys
import tempfile
import shutil
from nose.tools import assert_raises

from zoom.zoom_git_module import ZoomGitModule
from zoom.zoom_error import ZoomError

import zoom_test

TEST_REPO = 'https://github.com/ssagar0/test_repo.git'

class TestGitModule(zoom_test.ZoomTest):
    '''
    Test the zoom git module.
    '''

    def setup(self):
        '''
        Create a directory to test in. Also create arg and response dicts.
        '''
        self.cwd = os.getcwd()
        self.root = os.path.realpath(tempfile.mkdtemp())
        os.chdir(self.root)

        self.base_args = {'root': self.root,
                          'path': None,
                          'url': TEST_REPO}
        self.base_exp = {'rtype': 'git',
                         'path': None,
                         'url': TEST_REPO}

    def teardown(self):
        '''
        Blow away the tempdir
        '''
        os.chdir(self.cwd)
        shutil.rmtree(self.root)
        del self.root

    def assert_module_state(self):
        '''
        Create a zoom git module and assert the state is as expected.
        For constructing the module object, uses self.base_args.
        The expected state of the object is described via self.base_exp.
        '''
        module = ZoomGitModule(**self.base_args)
        assert(cmp(module.serialize(), self.base_exp) == 0)

    def test_serialize_plain(self):
        self.base_args.update({'path': 'test'})
        self.base_exp.update({'path': 'test',
                              'branch': 'master'})
        self.assert_module_state()

    def test_serialize_branch(self):
        self.base_args.update({'path': 'test',
                               'branch': 'feature/test'})
        self.base_exp.update({'path': 'test',
                              'branch': 'feature/test'})
        self.assert_module_state()

    def test_serialize_commit(self):
        self.base_args.update({'path': 'test',
                               'commit': '123456'})
        self.base_exp.update({'path': 'test',
                              'commit': '123456'})
        self.assert_module_state()

    def test_serialize_tag(self):
        self.base_args.update({'path': 'test',
                               'tag': 'v0.0.1'})
        self.base_exp.update({'path': 'test',
                              'tag': 'v0.0.1'})
        self.assert_module_state()

    def test_default_branch(self):
        self.base_args.update({'path': 'test'})
        module = ZoomGitModule(**self.base_args)
        assert(module.branch == 'master')
        assert(module.commit == None)
        assert(module.tag == None)

    def test_is_synced(self):
        module = ZoomGitModule(**self.base_args)
        assert(module.is_synced() == False)
        module.sync()
        assert(module.is_synced() == True)

    def test_sync_plain(self):
        module = ZoomGitModule(**self.base_args)
        module.sync()
        assert(os.path.exists(module.abspath))
        git_dir = os.path.join(module.abspath, '.git')
        assert(os.path.exists(git_dir))

    def test_sync_exact(self):
        module = ZoomGitModule(**self.base_args)
        with assert_raises(ZoomError):
            module.sync(exact=True)
        assert(module.is_synced() == False)

        # normal sync first
        module.sync()
        module.sync(exact=True)
        assert(module.is_synced() == True)

    def test_status(self):
        module = ZoomGitModule(**self.base_args)
        assert(module.status() == "repo not synced")
        module.sync()
        assert(module.status() != "repo not synced")

    def test_delete(self):
        module = ZoomGitModule(**self.base_args)
        module.sync()
        module.delete()
        assert(module.is_synced() == False)
        git_dir = os.path.join(module.abspath, '.git')
        assert(os.path.exists(git_dir) == False)

    def test_lock_commit(self):
        module = ZoomGitModule(**self.base_args)
        module.sync()
        old_commit = module.commit
        module.lock_commit()
        assert(module.commit is old_commit)
        assert(module.branch is None)
        assert(module.tag is None)

    def test_lock_commit_specific(self):
        module = ZoomGitModule(**self.base_args)
        module.sync()
        module.lock_commit('123456')
        assert(module.commit is '123456')
        assert(module.branch is None)
        assert(module.tag is None)

    def test_lock_tag(self):
        module = ZoomGitModule(**self.base_args)
        module.sync()
        module.lock_tag('v123')
        assert(module.commit is None)
        assert(module.branch is None)
        assert(module.tag is 'v123')
