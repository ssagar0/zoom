import os
import sys
import tempfile
import shutil
import contextlib
import yaml
from nose.tools import assert_raises

from zoom.zoom_project import ZoomProject
from zoom.zoom_io import ZOOM_FILE
from zoom.zoom_error import ZoomError

import zoom_test

TEST_FILE = 'https://raw.githubusercontent.com/ssagar0/test_repo/tag-1/readme.md'
TEST_FILE_PATH = 'test_file'
TEST_FILE_FILENAME = TEST_FILE.split('/')[-1]

TEST_REPO = 'https://github.com/ssagar0/test_repo.git'
TEST_REPO_PATH = 'test_repo'

class TestZoomProject(zoom_test.ZoomTest):
    '''
    Test the zoom project object.
    '''

    def setup(self):
        '''
        Create a directory to test in.
        '''
        self.cwd = os.getcwd()
        self.root = os.path.realpath(tempfile.mkdtemp())
        os.chdir(self.root)

    def teardown(self):
        '''
        Blow away the tempdir
        '''
        os.chdir(self.cwd)
        shutil.rmtree(self.root)
        del self.root

    @contextlib.contextmanager
    def zoom_proj(self):
        '''
        Context manager for a zoom project. Creates and saves in a tempdir.

        yields:
            ZoomProject: new, empty ZoomProject object
        '''
        zp = ZoomProject(create=True)
        yield zp

    def add_git_repo(self, zp, path=TEST_REPO_PATH, url=TEST_REPO):
        '''
        Add a git module to zp (ZoomProject)
        '''
        args = {'path': path, 'url': url}
        return zp.add_resource('git', args)

    def add_file(self, zp, path=TEST_FILE_PATH, url=TEST_FILE):
        '''
        Add a file dependency to zp (ZoomProject)
        '''
        args = {'path': path, 'url': url}
        return zp.add_resource('file', args)

    def test_init_create(self):
        with self.zoom_proj() as _:
            assert(os.path.join(self.root, ZOOM_FILE))

    def test_init_create_exists(self):
        with self.zoom_proj() as _:
            with assert_raises(ZoomError):
                zp = ZoomProject(create=True)

    def test_init_load(self):
        with self.zoom_proj() as zp:
            assert(zp.root == self.root)
            assert(zp.name == os.path.basename(self.root))

    def test_init_load_dne(self):
        with assert_raises(ZoomError):
            zp = ZoomProject()

    def test_save_empty(self):
        with self.zoom_proj() as zp:
            zf = os.path.join(self.root, ZOOM_FILE)
            zfc = None
            with open(zf) as f:
                zfc = yaml.load(f.read())
            assert(zfc['name'] == zp.name)
            # these will both be empty
            assert(zfc['resources']['dependencies'] == zp.deps)
            assert(zfc['resources']['modules'] == zp.modules)

    def test_add_module(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            zp.sync()
            synced_path = os.path.join(self.root, TEST_REPO_PATH)
            assert(os.path.exists(synced_path))

    def test_add_module_outside(self):
        with self.zoom_proj() as zp:
            with assert_raises(ZoomError):
                self.add_git_repo(zp, path=os.path.abspath(os.sep))

    def test_add_module_dupe(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            with assert_raises(ZoomError):
                self.add_git_repo(zp)

    def test_add_dep(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            zp.sync()
            synced_path = os.path.join(self.root, TEST_FILE_PATH,
                                       TEST_FILE_FILENAME)
            assert(os.path.exists(synced_path))

    def test_add_dep_dupe(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            with assert_raises(ZoomError):
                self.add_file(zp)

    def test_sync_empty(self):
        with self.zoom_proj() as zp:
            zp.sync()
            dir_contents = os.listdir(self.root)
            assert(dir_contents == [ZOOM_FILE])

    def test_sync_force(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            self.add_git_repo(zp)
            synced_git_path = os.path.join(self.root, TEST_REPO_PATH)
            synced_repo_git_dir = os.path.join(synced_git_path, '.git')
            # sync and remove git dir in git module
            zp.sync()
            assert(os.path.exists(synced_git_path))
            shutil.rmtree(synced_repo_git_dir)
            assert(not os.path.exists(synced_repo_git_dir))
            # re-sync and expect that path to get restored
            zp.sync(force=True)
            assert(os.path.exists(synced_repo_git_dir))

    def test_sync_exact(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            synced_git_path = os.path.join(self.root, TEST_REPO_PATH)
            synced_repo_git_head = os.path.join(synced_git_path, '.git', 'HEAD')
            # sync should fail due to a lack of an exact commit
            zp.sync(exact=True)
            assert(not os.path.exists(synced_git_path))
            # sync and verify this is master
            zp.sync()
            with open(synced_repo_git_head, 'r') as f:
                assert('master' in f.read())
            # sync and verify this is the commit
            master_sha = zp.get_modules()[0].commit
            zp.sync(exact=True)
            with open(synced_repo_git_head, 'r') as f:
                assert(master_sha == f.read().strip())

    def test_sync_multi(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            self.add_git_repo(zp)
            zp.sync()
            synced_git_path = os.path.join(self.root, TEST_REPO_PATH)
            synced_file_path = os.path.join(self.root, TEST_FILE_PATH,
                                            TEST_FILE_FILENAME)
            assert(os.path.exists(synced_git_path))
            assert(os.path.exists(synced_file_path))

    def test_sync_limit(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            self.add_git_repo(zp)
            zp.sync(limit=[TEST_REPO_PATH])
            synced_git_path = os.path.join(self.root, TEST_REPO_PATH)
            synced_file_path = os.path.join(self.root, TEST_FILE_PATH,
                                            TEST_FILE_FILENAME)
            assert(os.path.exists(synced_git_path))
            assert(not os.path.exists(synced_file_path))

    def test_get_resources(self):
        expected = (TEST_REPO, TEST_FILE)
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            self.add_file(zp)
            resources = zp.get_resources()
            for r in resources:
                assert(r.url in expected)

    def test_get_dependencies(self):
        with self.zoom_proj() as zp:
            self.add_file(zp)
            resources = zp.get_resources()
            for r in resources:
                assert(r.url == TEST_FILE)

    def test_get_modules(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            resources = zp.get_resources()
            for r in resources:
                assert(r.url == TEST_REPO)

    def test_foreach_module_pre_sync(self):
        with self.zoom_proj() as zp:
            resource = self.add_git_repo(zp)
            # should fail miserably
            results = zp.foreach_module('git status')
            assert(results[str(resource)]['retcode'] == 128)

    def test_foreach_module(self):
        with self.zoom_proj() as zp:
            resource = self.add_git_repo(zp)
            zp.sync()
            # should be synced to master
            results = zp.foreach_module('git status')
            assert('branch master' in results[str(resource)]['stdout'])

    def test_foreach_module_limit(self):
        with self.zoom_proj() as zp:
            repo1 = self.add_git_repo(zp)
            repo2 = self.add_git_repo(zp, path='test_repo_2')
            zp.sync(limit=[TEST_REPO_PATH])
            # should be synced to master
            assert(repo1.is_synced() == True)
            assert(repo2.is_synced() == False)

    def test_lock_commit(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            zp.sync()
            mod = zp.lock_commit(TEST_REPO_PATH, '123')
            assert(mod.commit == '123')

    def test_lock_tag(self):
        with self.zoom_proj() as zp:
            self.add_git_repo(zp)
            zp.sync()
            modules = zp.lock_tag('v123', limit=[TEST_REPO_PATH])
            print modules
            assert(modules[0].tag == 'v123')
