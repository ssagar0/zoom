import os
import sys
import tempfile
import shutil
from nose.tools import assert_raises

from zoom.zoom_file_dep import ZoomFileDep
from zoom.zoom_error import ZoomError

import zoom_test

TEST_FILE = 'https://raw.githubusercontent.com/ssagar0/test_repo/tag-1/readme.md'
TEST_FILENAME = TEST_FILE.split('/')[-1]

FAKE_FILE = 'http://1/2/3/file'
FAKE_FILENAME = FAKE_FILE.split('/')[-1]

class TestFileDep(zoom_test.ZoomTest):
    '''
    Test the zoom file dependency resource.
    '''

    def setup(self):
        '''
        Create a directory to test in. Also create arg and response dicts.
        '''
        self.cwd = os.getcwd()
        self.root = os.path.realpath(tempfile.mkdtemp())
        os.chdir(self.root)

        self.base_args = {'root': self.root,
                          'path': None,
                          'url': TEST_FILE}
        self.base_exp = {'rtype': 'file',
                         'filename': TEST_FILENAME,
                         'path': None,
                         'url': TEST_FILE}

    def teardown(self):
        '''
        Blow away the tempdir
        '''
        os.chdir(self.cwd)
        shutil.rmtree(self.root)
        del self.root

    def assert_dep_state(self):
        '''
        Create a zoom file dep and assert the state is as expected.
        For constructing the dep object, uses self.base_args.
        The expected state of the object is described via self.base_exp.
        '''
        dep = ZoomFileDep(**self.base_args)
        print dep.serialize()
        print self.base_exp
        assert(cmp(dep.serialize(), self.base_exp) == 0)

    def test_serialize_plain(self):
        self.base_args.update({'path': 'test'})
        self.base_exp.update({'path': 'test'})
        self.assert_dep_state()

    def test_serialize_no_path(self):
        self.base_args.update()
        self.base_exp.update({'path': ''})
        self.assert_dep_state()

    def test_is_synced(self):
        dep = ZoomFileDep(**self.base_args)
        assert(dep.is_synced() == False)
        dep.sync()
        assert(dep.is_synced() == True)

    def test_sync_error(self):
        self.base_args.update({'url': FAKE_FILE})
        self.base_exp.update({'filename': FAKE_FILENAME})
        dep = ZoomFileDep(**self.base_args)
        with assert_raises(ZoomError):
            dep.sync()

    def test_sync(self):
        dep = ZoomFileDep(**self.base_args)
        assert(os.path.exists(dep.identifier) == False)
        dep.sync()
        assert(os.path.exists(dep.identifier))

    def test_status(self):
        dep = ZoomFileDep(**self.base_args)
        assert(dep.status() == "file not synced")
        dep.sync()
        assert(dep.status() != "file not synced")

    def test_delete(self):
        dep = ZoomFileDep(**self.base_args)
        dep.sync()
        dep.delete()
        assert(dep.is_synced() == False)
