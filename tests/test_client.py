import mock
from click.testing import CliRunner
from nose.tools import assert_raises

import zoom.zoom_client as zc
from zoom.zoom_error import ZoomError

import zoom_test

ADDGIT = 'add-git'
ADDFILE = 'add-file'
REVLOCK = 'rev-lock'
TAGLOCK = 'tag-lock'

REPO_URL = 'https://github.com/ssagar0/test_repo.git'
REPO_PATH = 'testrepo'
REPO_BRANCH = 'feature/test'
REPO_COMMIT = 'abc'
REPO_TAG = 'v1.0'
FILE_URL = 'http://somewhereintheuniverse/probably'
FILE_PATH = 'testfile'
FILE_NAME = 'probably_real'
FOREACH_CMD = 'ls *'
LIMIT1 = 'limit1'
LIMIT2 = 'limit2'

class TestClient(zoom_test.ZoomTest):
    '''
    Test the zoom client.
    '''

    def setup(self):
        '''
        create a CliRunner object and hijack
        zc.load_zoom_proj to return a mock
        '''
        self.runner = CliRunner()
        mzp = mock.MagicMock()

        def hijack(*args, **kwargs):
            return mzp
        zc.load_zoom_proj = hijack
        self.mzp = mzp

    def teardown(self):
        '''
        delete the CliRunner object
        '''
        del self.runner

    def assert_cli_success(self, command):
        '''
        assert command was successfully called

        args:
            command: command to send to cli
        '''
        result = self.runner.invoke(zc.cli, command)
        print result.exit_code
        print result.exception
        print result.output

        assert(result.exception == None)
        assert(result.exit_code == 0)

    def assert_cli_fail(self, command):
        '''
        assert command failed

        args:
            command: command to send to cli
        '''
        result = self.runner.invoke(zc.cli, command)
        print result
        assert(result.exit_code == 1)

    def assert_parser_fail(self, command):
        '''
        assert parser failed to call func due to a malformed command

        args:
            command: command to send to cli
        '''
        result = self.runner.invoke(zc.cli, command)
        print result
        assert(result.exit_code == 2)

    #
    # test the init command
    #

    def test_init(self):
        command = ['init']
        func = self.mzp
        self.assert_cli_success(command)

    def test_init_fail(self):
        # special case, need to re-hijack in order to simulate
        # a failure to create a ZoomProject
        def fail(*args, **kwargs):
            raise ZoomError
        zc.load_zoom_proj = fail
        command = ['init']
        self.assert_cli_fail(command)

    def test_load_fail(self):
        # special case, need to re-hijack in order to simulate
        # a failure to load a ZoomProject
        def fail(*args, **kwargs):
            raise ZoomError
        zc.load_zoom_proj = fail
        # any non-init command will suffice
        command = ['status']
        self.assert_cli_fail(command)

    #
    # test general add resource commands
    #

    def test_add_resource_fail(self):
        command = [ADDGIT, REPO_URL]
        self.mzp.add_resource.side_effect = ZoomError
        self.assert_cli_fail(command)

    #
    # test the add git command
    #

    def test_add_git_nothing(self):
        command = [ADDGIT]
        self.assert_parser_fail(command)

    def test_add_git_url(self):
        command = [ADDGIT, REPO_URL]
        self.assert_cli_success(command)
        expected = {'url': REPO_URL, 'path': None,
                    'branch': None, 'commit': None, 'tag': None}
        self.mzp.add_resource.assert_called_with('git', expected)

    def test_add_git_url_path(self):
        command = [ADDGIT, REPO_URL, REPO_PATH]
        self.assert_cli_success(command)
        expected = {'url': REPO_URL, 'path': REPO_PATH,
                    'branch': None, 'commit': None, 'tag': None}
        self.mzp.add_resource.assert_called_with('git', expected)

    def test_add_git_url_tag(self):
        command = [ADDGIT, '--tag', REPO_TAG, REPO_URL]
        self.assert_cli_success(command)
        expected = {'url': REPO_URL, 'path': None,
                    'branch': None, 'commit': None, 'tag': REPO_TAG}
        self.mzp.add_resource.assert_called_with('git', expected)

    def test_add_git_url_branch(self):
        command = [ADDGIT, '--branch', REPO_BRANCH, REPO_URL]
        self.assert_cli_success(command)
        expected = {'url': REPO_URL, 'path': None,
                    'branch': REPO_BRANCH, 'commit': None, 'tag': None}
        self.mzp.add_resource.assert_called_with('git', expected)

    def test_add_git_url_commit(self):
        command = [ADDGIT, '--commit', REPO_COMMIT, REPO_URL]
        self.assert_cli_success(command)
        expected = {'url': REPO_URL, 'path': None,
                    'branch': None, 'commit': REPO_COMMIT, 'tag': None}
        self.mzp.add_resource.assert_called_with('git', expected)

    #
    # test the add file command
    #

    def test_add_file_nothing(self):
        command = [ADDFILE]
        self.assert_parser_fail(command)

    def test_add_file_url(self):
        command = [ADDFILE, FILE_URL]
        self.assert_cli_success(command)
        expected = {'url': FILE_URL, 'path': None, 'filename': None}
        self.mzp.add_resource.assert_called_with('file', expected)

    def test_add_file_url_path(self):
        command = [ADDFILE, FILE_URL, FILE_PATH]
        self.assert_cli_success(command)
        expected = {'url': FILE_URL, 'path': FILE_PATH, 'filename': None}
        self.mzp.add_resource.assert_called_with('file', expected)

    def test_add_file_url_filename(self):
        command = [ADDFILE, '--filename', FILE_NAME, FILE_URL]
        self.assert_cli_success(command)
        expected = {'url': FILE_URL, 'path': None, 'filename': FILE_NAME}
        self.mzp.add_resource.assert_called_with('file', expected)

    #
    # test the foreach command
    #

    def test_foreach_nothing(self):
        command = ['foreach']
        self.assert_parser_fail(command)

    def test_foreach_command(self):
        command = ['foreach', FOREACH_CMD]
        mock_r = {
            'mock': {
                'retcode': 0,
                'stdout': 'out',
                'stderr': 'err'
            }
        }
        self.mzp.foreach_module.return_value = mock_r
        self.assert_cli_success(command)
        self.mzp.foreach_module.assert_called_with(FOREACH_CMD, limit=[])

    def test_foreach_command_limit(self):
        command = ['foreach', FOREACH_CMD, LIMIT1]
        self.mzp.foreach_module.return_value = {}
        self.assert_cli_success(command)
        self.mzp.foreach_module.assert_called_with(FOREACH_CMD, limit=[LIMIT1])

    #
    # test the list command
    #

    def test_list(self):
        command = ['list']
        mock_r = mock.MagicMock()
        self.mzp.get_resources.return_value = [mock_r]
        self.assert_cli_success(command)
        assert(self.mzp.get_resources.called)
        assert(mock_r.__str__.called)

    #
    # test the status command
    #

    def test_status(self):
        command = ['status']
        mock_r = mock.MagicMock()
        mock_r.status.return_value = 'some text'
        self.mzp.get_resources.return_value = [mock_r]
        self.assert_cli_success(command)
        assert(self.mzp.get_resources.called)
        assert(mock_r.__str__.called)
        assert(mock_r.status.called)

    #
    # test the sync command
    #

    def test_sync_basic(self):
        command = ['sync']
        self.assert_cli_success(command)
        expected = {'exact': False, 'force': False, 'limit': [],
                    'sync_cb': zc.sync_cb, 'error_cb': zc.sync_err_cb}
        self.mzp.sync.assert_called_with(**expected)

    def test_sync_exact(self):
        command = ['sync', '--exact']
        self.assert_cli_success(command)
        expected = {'exact': True, 'force': False, 'limit': [],
                    'sync_cb': zc.sync_cb, 'error_cb': zc.sync_err_cb}
        self.mzp.sync.assert_called_with(**expected)

    def test_sync_force(self):
        command = ['sync', '--force']
        self.assert_cli_success(command)
        expected = {'exact': False, 'force': True, 'limit': [],
                    'sync_cb': zc.sync_cb, 'error_cb': zc.sync_err_cb}
        self.mzp.sync.assert_called_with(**expected)

    def test_sync_limit(self):
        command = ['sync', LIMIT1]
        self.assert_cli_success(command)
        expected = {'exact': False, 'force': False, 'limit': [LIMIT1],
                    'sync_cb': zc.sync_cb, 'error_cb': zc.sync_err_cb}
        self.mzp.sync.assert_called_with(**expected)

    #
    # test the rev-lock command
    #

    def test_rev_lock_blank(self):
        command = [REVLOCK]
        self.assert_parser_fail(command)

    def test_rev_lock_module(self):
        command = [REVLOCK, REPO_PATH]
        mock_r = mock.MagicMock()
        mock_r.__str__.return_value = REPO_PATH
        mock_r.commit.return_value = REPO_COMMIT
        self.mzp.lock_commit.return_value = mock_r
        self.assert_cli_success(command)

        assert(self.mzp.lock_commit.called)
        expected = (REPO_PATH, None)
        self.mzp.lock_commit.assert_called_with(*expected)

    def test_rev_lock_module_commit(self):
        command = [REVLOCK, REPO_PATH, REPO_COMMIT]
        mock_r = mock.MagicMock()
        mock_r.__str__.return_value = REPO_PATH
        mock_r.commit.return_value = REPO_COMMIT
        self.mzp.lock_commit.return_value = mock_r
        self.assert_cli_success(command)

        assert(self.mzp.lock_commit.called)
        expected = (REPO_PATH, REPO_COMMIT)
        self.mzp.lock_commit.assert_called_with(*expected)

    def test_rev_lock_fail(self):
        command = [REVLOCK, REPO_PATH]
        self.mzp.lock_commit.side_effect = ZoomError
        self.assert_cli_fail(command)
        assert(self.mzp.lock_commit.called)

    #
    # test the rev-lock command
    #

    def test_tag_lock_blank(self):
        command = [TAGLOCK]
        self.assert_parser_fail(command)

    def test_tag_lock_module(self):
        command = [TAGLOCK, REPO_TAG]
        mock_r = mock.MagicMock()
        mock_r.__str__.return_value = REPO_PATH
        mock_r.tag.return_value = REPO_TAG
        self.mzp.lock_tag.return_value = [mock_r]
        self.assert_cli_success(command)

        assert(self.mzp.lock_tag.called)
        self.mzp.lock_tag.assert_called_with(REPO_TAG, limit=[])

    def test_rev_lock_module_commit(self):
        command = [TAGLOCK, REPO_TAG, REPO_PATH]
        mock_r = mock.MagicMock()
        mock_r.__str__.return_value = REPO_PATH
        mock_r.tag.return_value = REPO_TAG
        self.mzp.lock_tag.return_value = [mock_r]
        self.assert_cli_success(command)

        assert(self.mzp.lock_tag.called)
        self.mzp.lock_tag.assert_called_with(REPO_TAG, limit=[REPO_PATH])

    def test_rev_lock_fail(self):
        command = [TAGLOCK, REPO_TAG]
        self.mzp.lock_tag.side_effect = ZoomError
        self.assert_cli_fail(command)
        assert(self.mzp.lock_tag.called)
