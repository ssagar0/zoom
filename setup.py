from distutils.core import setup
from zoom.__version__ import __version__

setup(
    name = 'zoom',
    packages = ['zoom'],
    version = __version__,
    description = 'Distributed project management',
    author = 'Sunny Sagar',
    author_email = 'trexinabluecape@gmail.com',
    url = 'https://bitbucket.org/ssagar0/zoom',
    entry_points = {
        'console_scripts': [
            "zoom = zoom:entrypoint"
        ]
    },
)
